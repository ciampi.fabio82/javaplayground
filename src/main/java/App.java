import java.io.IOException;
import java.util.Properties;

import test.file.PropertyFileReader;

class App {
    public static void main(String args[]) {

        try {
//            Properties fileProp = PropertyFileReader.getPropertiesFromResourceFile();
            Properties fileProp = PropertyFileReader.getPropertiesFromFile();
            System.out.println(fileProp);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}