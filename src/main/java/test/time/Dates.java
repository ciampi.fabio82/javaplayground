package test.time;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.Callable;

public class Dates {

	public static void main(String[] args) throws Exception {

		System.out.println(String.format("now: %s", LocalDateTime.now()));

		/* %s: string %n: print in a new line */
		System.out.printf("Apr 15, 1994 @ 11:30am: %s%n", LocalDateTime.of(1994, Month.APRIL, 15, 11, 30));

		System.out.printf("now (from Instant): %s%n", LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()));

		//now() method provides the current date and time using the system clock and the default time zone. 
		LocalDateTime dt = LocalDateTime.now();
		
		
		ZoneId zone = ZoneId.of("Etc/UTC");
		//combine local time with a time zone. Specify the timezone for localdatetime. It doesn't transform it!
		ZonedDateTime zdtUtc = dt.atZone(zone);
		ZoneOffset offset = zdtUtc.getOffset();
		System.out.println(String.format("UTC time now: %s , offset: %n", zdtUtc, offset.getTotalSeconds()));
		
		//transform to a different timezone
		ZonedDateTime zdtBerlin = zdtUtc.withZoneSameInstant(ZoneId.of("Europe/Berlin"));
		System.out.println(String.format("Beline time now: %s , offset: %n", zdtBerlin, zdtBerlin.getOffset().getTotalSeconds()));

		//ZonedDateTime constructor
		ZonedDateTime zoneDataTime = ZonedDateTime.of(dt, ZoneId.of("Europe/Berlin"));
		OffsetDateTime offsetDate = OffsetDateTime.of(dt, ZoneOffset.of("+02:00"));
		System.out.println("zoneDataTime: " + zoneDataTime + " offsetDateTime: " + offsetDate);

		//seconds from epoch
		long secondsFromEpoch = Instant.ofEpochSecond(0L).until(Instant.now(), ChronoUnit.MILLIS);
		long nowMs = Instant.now().toEpochMilli();
		System.out.println("secondsFromEpoch: " + secondsFromEpoch + " or: " + nowMs);

		//LocalTime from epoch time
		Instant timestamp = Instant.ofEpochMilli(secondsFromEpoch);
		System.out.println("current time: " + LocalDateTime.ofInstant(timestamp, ZoneId.systemDefault()));

		//Custom formatting 
		DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d yyyy  hh:mm.ss.SSSS a");
	    System.out.printf("LEAVING1:  %s%n", zoneDataTime.format(format));
	    //Pre-defined formatter
	    System.out.printf("LEAVING2:  %s%s%n", zoneDataTime.format(DateTimeFormatter.ISO_INSTANT), zoneDataTime.getOffset());
	    System.out.printf("LEAVING3:  %s%n", dt.format(DateTimeFormatter.BASIC_ISO_DATE));
	
	
	    //Duration and period
	    Instant t1 = Instant.now().plus(Duration.ofSeconds(2));
	    Instant t2 = Instant.now().plus(4, ChronoUnit.SECONDS);
	    Duration t3 = Duration.between(t1, t2); //return a time period
	    long ms = ChronoUnit.MILLIS.between(t2,t1);  //returns directly as unit
	    
	    //Clock - system default vs specific
	    Clock utcClock = Clock.systemUTC();
	    System.out.println(LocalDateTime.now(utcClock));
	    System.out.println(LocalDateTime.now());
	}
}
