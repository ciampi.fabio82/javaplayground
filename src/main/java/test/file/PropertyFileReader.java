package test.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileReader {

    public static Properties getPropertiesFromResourceFile() throws FileNotFoundException, IOException {
        String propFileName = "config.properties";

        InputStream inputStream = PropertyFileReader.class.getClassLoader().getResourceAsStream(propFileName);

        if (inputStream == null) {
            throw new FileNotFoundException("property file: " + propFileName + " not found in classPath");
        }

        Properties prop = new Properties();

        prop.load(inputStream);
        return prop;
    }

    public static Properties getPropertiesFromFile() throws FileNotFoundException, IOException {

        String fileName = "/home/fabio/config.properties";

        Properties prop = new Properties();

        try (BufferedReader bufferReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferReader.readLine()) != null) {

                if (line.startsWith("#"))
                    continue;

                String[] words = line.split("=");

                if (words.length == 2)
                    prop.put(words[0], words[1]);
            }
        }
        return prop;
    }
}
