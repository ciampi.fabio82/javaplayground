package test.file;

import java.io.FileWriter;
import java.io.IOException;

public class PropertyFileWriter {

    public static void writePropertiesFile() throws IOException {

        String fileName = "/home/fabio/config.properties";

        // use FileOutputStream for bytes, FileWrite for strings
        FileWriter writer = new FileWriter(fileName);
        writer.write("#comment");
        writer.write(System.getProperty("line.separator"));
        writer.write("prop1=value1");
        writer.write(System.getProperty("line.separator"));
        writer.write("prop2=value2");

        writer.close();
    }
}
