package test.thread;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

class CallableExample implements Callable {

    public Object call() throws Exception {
        Random generator = new Random();
        Integer randomNumber = generator.nextInt(5);

        Thread.sleep(randomNumber * 1000);

        return randomNumber;
    }

}

/*
 * To create a thread:
 * 1) either extend the class Thread overriding the function run
 * 2) create a class which implements Runnable and create a Thread passing
 * an instance of that class
 */
public class TheadWithFuture {

    public static void main(String[] args) throws Exception {
        Callable callable = new CallableExample();

        // FutureTask is a concrete class that implements both interfaces Runnable and
        // Future. It can wrap a Callable or Runnable object
        FutureTask randomNumberTask = new FutureTask(callable);

        // As it implements Runnable, create Thread with FutureTask
        Thread thread = new Thread(randomNumberTask);

        thread.start();

        // It blocks till a result is written there
        System.out.println("Thread terminated with result: " + randomNumberTask.get());
    }
}
