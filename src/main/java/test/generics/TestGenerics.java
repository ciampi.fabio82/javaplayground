package test.generics;

import java.util.ArrayList;
import java.util.List;

public class TestGenerics {

    interface  Container<T> {
        void print(T member);
    }

    class Store<T> implements  Container<String>
    {
        T member;
        public void setMember(T member) {
            this.member = member;
        }

        @Override
        public void print(String member) {
            System.out.println(member);
        }

        T getMember(){
            return member;
        }
    }

    void compare(List<?> list1, List<?> list2){
        list1.contains(3);
    }

    class Bookmarks
    {
    }
    class Bookmark extends Bookmarks
    {

    }

    <T extends Bookmarks> void aggregate(List<T> list){
        //T bla = new T();
    }

    interface MyFactory<T>
    {
        T newObject();
    }

    class MyClass<T>
    {
        T field;
        public MyClass(MyFactory<T> factory)
        {
            field = factory.newObject();
        }
    }

    class StringBuilder implements MyFactory<String>
    {
        @Override
        public String newObject() {
            return new String();
        }
    }

    public void Foo() {

        MyClass<String> myClass = new MyClass<>(new StringBuilder());

        aggregate(new ArrayList<Bookmark>());
        //aggregate(new ArrayList<Object>());

    }
}
