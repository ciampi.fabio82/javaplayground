package test.generics;

import java.util.List;

/*
 * https://www.baeldung.com/java-generics
 * To understand why primitive data types don't work, let's remember that generics are a compile-time feature, 
 * meaning the type parameter is erased and all generic types are implemented as type Object.
*/
public class Generic {

	public interface Pair<K, V> {
		public K getKey();

		public V getValue();
	}

	public class MyPair<K, V> implements Pair<K, V> {
		private K key;
		private V value;

		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		};
	}

	public class Box<T> {
		private T obj;

		public T getObj() {
			return obj;
		}

		public void set(T obj) {
			this.obj = obj;
		}

		// <U>: type parameter introduced in a method
		// extend, super: added boundes
		public <U extends Number> void inspect(U u) {
			u.getClass().getName();
		}

	}

	// Wild-card: make it less restrictive, more flexible
	// extends == means extend or implement
	// Instead of accepting only one type like for inspect, it is not bound to a
	// type specific
	public void sumOfNumber(List<? extends Number> numbers) {
		double d = 0.0;
		for (Number n : numbers) {
			d += n.doubleValue();
		}
	}

	public void testSumOfNumber() {
		// initialize array -> no new ArrayList<>(1,2,3) but Arrays.asList(1, 2, 3);

		// ? super Number -> it forces to be as generic as possible in the
		// implementation of the function. Therefore it forces to use Object. Used for
		// generic "setters"
		// ? extend Number -> used for generic "getters"
		// usually ? is used with collections
	}
}
