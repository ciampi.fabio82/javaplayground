package test.callback;

interface StateTax {

    public int calculate(boolean doCheck);
}

class ItalyStateTax implements StateTax {

    public int calculate(boolean doCheck) {
        return 10;
    }
}

class GermanyStateTax implements StateTax {

    public int calculate(boolean doCheck) {
        return 11;
    }
}

public class TaxCalculator {

    // callback type must be expressed as an interface
    static public int execute(int income, StateTax stateTax) {
        return income + stateTax.calculate(true);
    }

}
