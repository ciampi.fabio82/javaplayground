package test.callback;

import java.time.Instant;

interface InterestingEvent {
    public void notificationCallback();
}

class EventNotifier {
    private InterestingEvent m_event;

    public void registerEvent(InterestingEvent event) {
        m_event = event;
    }

    public void Notify() {
        m_event.notificationCallback();
    }
}

class GermanClass implements InterestingEvent {

    public GermanClass(EventNotifier notifier) {
        notifier.registerEvent(this);
    }

    public void notificationCallback() {
        System.out.println("German class started now: " + Instant.now().toString());
    }
}

public class NotifyRegisteredEvents {
    public static void main(String args[]) {

        EventNotifier en = new EventNotifier();
        new GermanClass(en);

        en.Notify();

    }
}