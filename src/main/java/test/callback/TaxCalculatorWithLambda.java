package test.callback;

public class TaxCalculatorWithLambda {

    interface StateTax2 {

        public int calculate(boolean doCheck);
    }

    // callback type must be expressed as an interface
    static public int execute(int income, StateTax2 stateTax) {
        return income + stateTax.calculate(true);
    }

    public static void main(String args[]) {

        // anonymous class
        execute(300, new StateTax2() {
            public int calculate(boolean doCheck) {
                return doCheck ? 11 : 12;
            }
        });

        // lambda function
        execute(300, doCheck -> doCheck ? 400 : 300);

        StateTax2 germanTax = (doCheck) -> doCheck ? 401 : 301;
        execute(300, germanTax);

    }
}
