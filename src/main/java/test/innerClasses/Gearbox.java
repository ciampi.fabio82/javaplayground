package test.innerClasses;

import java.util.ArrayList;

public class Gearbox {

    private ArrayList<Gear> gears;
    private int maxGears;
    private int currentGear = 0;

    public Gearbox(int maxGears)
    {
        this.maxGears = maxGears;
        this.gears = new ArrayList<>();

        Gear neutral = new Gear(0, 0.0);
        neutral.setGearNumber();
        this.gears.add(neutral);
    }

    //used only coupled with the gearBox access to all the methods of the outer class
    //usually it is made private
    public class Gear
    {
        private int gearNumber;
        private double ration;

        public Gear(int gearNumber, double ration) {
            this.gearNumber = gearNumber;
            this.ration = ration;

            //so to access to a private field of the outer class
            Gearbox.this.currentGear = 1;
        }

        //! must be public to be accessed from the outer class
        public void setGearNumber(){
            this.gearNumber = 1;
        }
    }

    //local classes; scope restricted to a block

    //anonymous classes
}
