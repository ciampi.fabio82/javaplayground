package test.innerClasses;

import java.util.Scanner;

public class App {

    private static Scanner scanner = new Scanner(System.in);
    private static Button btnPrint = new Button("Print");

    public static void main(String[] args)
    {
        Gearbox mcLaren = new Gearbox(6);

        //Inner class
        //! you need an instance of the outer class to create the inner class
        Gearbox.Gear first = mcLaren.new Gear(1, 11.0);

        //Local class
        class ClickListener implements Button.OnClickListener {
            public ClickListener(){
                System.out.println("I've been attached");
            }

            @Override
            public void onClick(String title) {
                System.out.println(title + " was clicked!");
            }
        }

        btnPrint.setOnClickListener(new ClickListener());
        listen();

        //Anonymous class
        btnPrint.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(String title) {
                System.out.println(title + " was clicked!");
            }
        });
    }

    private static void listen(){
        boolean quit = false;
        while(!quit){

            //get the token (till a whitespace)
            int choice = scanner.nextInt();
            //skip the remaining content after the token
            scanner.nextLine();
            switch (choice)
            {
                case 0:
                    quit = true;
                    break;
                case 1:
                    btnPrint.onClick();
            }
        }
    }

}
