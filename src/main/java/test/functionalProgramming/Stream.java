package test.functionalProgramming;

import java.io.File;
import java.io.FileFilter;

//function interface 
interface Function<T, R> {
    R apply(T t);
}

class DaysInMonth {
    public void example1() {
        // pure function. It doesn't depend on the state
        // use anonymous inner class
        Function<Integer, Integer> dim = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer month) {
                return new Integer[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }[month];
            }
        };
        System.out.printf("April: %d%n", dim.apply(3));
        System.out.printf("August: %d%n", dim.apply(7));
    }

    public void example2() {
        // a higher-order function receives function arguments and/or returns a function
        // result. Java
        File[] txtFiles = new File(".").listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getAbsolutePath().endsWith("txt");
            }
        });
    }

}
