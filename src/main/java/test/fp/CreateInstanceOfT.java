package test.fp;

import java.util.function.Supplier;

public class CreateInstanceOfT<T> {

    private T type;

    public CreateInstanceOfT(Supplier<T> supplier)
    {
        type = supplier.get();
    }

    static void GetCreateInstanceOfT1()
    {
//        Supplier<String> supplier = String::new;
//        CreateInstanceOfT<String> myInstance = new CreateInstanceOfT(supplier);

        CreateInstanceOfT<String> myInstance = new CreateInstanceOfT(String::new);
    }
}
