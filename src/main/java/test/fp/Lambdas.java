package test.fp;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;


public class Lambdas {

    public void Foo(){

        //Composition with andThen
        Predicate<String> filter = d -> d.contains("bla");
        Function<String, String> docProcessor1 = input -> input == "bla" ? "bla" : "ciao";
        Function<String, String> docProcessor2 = input -> input == "bla" ? "bla" : "ciao";

        docProcessor1.andThen(docProcessor2);
        docProcessor1.apply("ciao");
    }

    class MyClass {
        public String removeHtmlTag() {return null;}
    }

    /*
        Methods referencing
        In a method reference, you place the object (or class) that contains the method before the
        :: operator and the name of the method after it without arguments.

        A method reference to a static method.
            (args) -> Class.staticMethod(args)
            Class::staticMethod

        A method reference to an instance method of an object of a particular type.
            (obj, args) -> obj.instanceMethod(args)
            ObjectType::instanceMethod

            Java in the apply method, for this method reference, will know that the first argument has to be
            used to call the function instanceMethod

        A method reference to an instance method of an existing object.
            (args) -> obj.instanceMethod(args)
            obj::instanceMethod

        A method reference to a constructor.
            (args) -> new ClassName(args)
            ClassName::new

    */
    public void Zoo(){

        //with Filter
        String myString = "My house is beautiful. Ciao a tutti!";

        Predicate<String> filter = myString::contains;      //<instanceName::methodName>
        filter.test("ciao");

        //with Function
        MyClass myClass = new MyClass();

        Function<MyClass, String> docProcessor1 = input -> input.removeHtmlTag();

        //alternative
        class Indexer {
            public String removeHtmlTag()
            {
                return null;
            }
        }

        Function<MyClass, String> docProcessor2 = MyClass::removeHtmlTag; //<className::methodName>

        docProcessor1.apply(myClass);

        //with BiFunction
        BiFunction<String, String, Boolean> biFunction = (s1, s2) -> s1.contains(s2);
        biFunction.apply("My name is Fabio", "Fabio");

        BiFunction<String, String, Boolean> biFunctionWithMethodRef = String::contains;
        biFunctionWithMethodRef.apply("My name is Fabio", "Fabio");

    }
}

