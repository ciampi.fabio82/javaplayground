package test.fp;

import java.util.ArrayList;
import java.util.List;

public class MyStream {

    class Indexer
    {
        public String stripHtmlCode(String input)
        {
            return null;
        }

        public String removeHtmlCode(String input)
        {
            return null;
        }

    }
    public void testStream()
    {
        Indexer indexer = new Indexer();
        List<String> documents = new ArrayList<>();

        //we are processing one document at the time!
        documents.stream()
                //Cannot work with method reference because we need to pass the "ciao" string
                .filter(s -> s.contains("ciao"))
                .map(indexer::stripHtmlCode)
                .map(s -> indexer.removeHtmlCode(s))
                .forEach(System.out::println);

        documents.stream()
            .findAny().ifPresent(System.out::println);

        String result = documents.stream()
                .findAny().orElseGet(() -> new String("ciao"));

        /*
            Stream.of(doc1, doc2, doc3)
            Arrays.stream(new Documents[]{doc1, doc2, doc3}
         */
    }
}
